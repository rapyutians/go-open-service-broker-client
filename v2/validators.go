package v2

import (
	"github.com/asaskevich/govalidator"
	"regexp"
)

const (
	IdCustomTypeKey        = "id"
)

func init() {
	govalidator.CustomTypeTagMap.Set(IdCustomTypeKey, govalidator.CustomTypeValidator(validateIds))
}

func validateIds(i interface{}, context interface{}) bool {
	if i == nil {
		return false
	}
	var id string
	id, ok := i.(string)
	if !ok {
		idPtr, ok := i.(*string)
		if !ok {
			opPtr, ok := i.(*OperationKey)
			if !ok {
				return false
			}
			id = string(*opPtr)
		} else {
			id = *idPtr
		}
	}
	if match, _ := regexp.MatchString(`^[a-zA-Z0-9][a-zA-Z0-9-]*$`, id); match {
		return true
	}
	return false
}
